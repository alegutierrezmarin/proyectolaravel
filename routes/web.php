<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// llamada para mostrar respuesta
Route::get('/prueba', function(){
    return response('<h1>Prueba de funcionamiento desde el controlador</h1>', 200);
});

// creacion de una pagina basica desde una ruta
Route::get('/saludo', function(){
    return response('<h2>Prueba de Funcionamiento</h2>', 200)
    ->header('Content-Type', 'text/html')
    ->header('foo', 'bar');
});

// pasar parametros en el navegador
Route::get('/post/{id}', function($id){
    ddd($id);
    return response('Post Nro.: '. $id);
})->where('id', '[0-9]+');

// llamada a metodos request
Route::get('/busqueda', function (Request $request) {
    dd($request);
    dd($request->name.' '.$request->city);
});

// creacion de un json
Route::get('/posts', function(){
    return response()->json([
        'posts'=>[
            'title'=>'post de prueba'

        ]
    ]);
});
